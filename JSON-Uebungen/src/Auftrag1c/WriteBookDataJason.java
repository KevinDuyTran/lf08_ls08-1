package Auftrag1c;
/*
{
	"titel": "XQuery Kick Start",
	"verlag": {
		"name": "Sams",
		"Ort": "Indianapolis"
	},
	"autoren": [
		"James McGovern",
		"Per Bothner",
		"Kurt Cagle",
		"James Linn"
	]
}
*/
import java.io.*;
import java.util.ArrayList;

import javax.json.*;
import javax.json.spi.*;

public class WriteBookDataJason {

	public static void main(String[] args) {
		
		ArrayList<String> autorenliste = new ArrayList<String>();
	    autorenliste.add("James McGovern");
	    autorenliste.add("Per Bothner");
	    autorenliste.add("Kurt Cagle");
	    autorenliste.add("James Linn");    
	    Buch buch = new Buch("XQuery Kick Start", autorenliste);
		
		JsonObjectBuilder  builder = Json.createObjectBuilder();
		builder.add("titel", buch.getTitel());
		
		JsonObjectBuilder  verlag = Json.createObjectBuilder();
		verlag.add("name","Sams");
		verlag.add("Ort","Indianapolis");
		
		builder.add("verlag", verlag);
	
		
		JsonArrayBuilder autorArray = Json.createArrayBuilder();
		for (String autor : buch.getAutorenliste()) {
			autorArray.add(autor);
		}
		builder.add("autoren", autorArray);	
	    
		JsonObject jo = builder.build();
		
		try {
			FileWriter fw = new FileWriter("book1c.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(jo);
			fw.close();
			jw.close();
		}
		catch(Exception ex) {
			ex.printStackTrace();
		}

	}

}
