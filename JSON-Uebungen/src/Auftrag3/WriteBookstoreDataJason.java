package Auftrag3;

/*
    {
		"name": "OSZIMT Buchhandlung",
		"tel": "030-225027-800",
		"fax": "030-225027-809",
		"adresse": {
			"strasse": "Haarlemer Straße",
			"hausnummer": "23-27",
			"plz": "12359",
			"ort": "Berlin"
		}
	}

*/
import java.util.*;
import java.io.*;
import javax.json.*;
import javax.json.spi.*;

public class WriteBookstoreDataJason {

	public static void main(String[] args) {
		
		

	   // add your code here
		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("name","OSZIMT Buchhandlung");
		builder.add("tel","030-225027-800");
		builder.add("fax","030-225027-809");
		
		JsonObjectBuilder hndlrAdresse = Json.createObjectBuilder();
		hndlrAdresse.add("strasse","Haarlemer Stra�e" );
		hndlrAdresse.add("hausnummer", "23-27");
		hndlrAdresse.add("plz", "12359");
		hndlrAdresse.add("ort","Berlin");
		
		builder.add("adresse", hndlrAdresse);
		
		JsonObject jo = builder.build();
		
		try {
			FileWriter fw = new FileWriter("haendler3.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(jo);
			jw.close();
			fw.close();
			
		}catch(Exception ex) {
			ex.printStackTrace();
		}

	}

}
