package Auftrag3;

 /* Arbeitsauftrag:  Lesen Sie alle Angaben des Buches "Java ist auch eine Insel" 
 *					 aus der Datei "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *
 *                   Ausgabe soll wie folgt aussehen:
 *                    titel:  Java ist auch eine Insel   
 *					  vorname:  Christian 
 *                    nachname:  Ullenboom 
 */

import javax.xml.parsers.*;
import org.w3c.dom.*;

public class ReadBookstoreData3 {

	public static void main(String[] args) {

		try {
			// Name der Datei: "src/Auftrag3/buchhandlung.xml"
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("src/Auftrag3/buchhandlung.xml");
			
			NodeList titelListe = doc.getElementsByTagName("titel");
			Node titel = titelListe.item(0);
			
			NodeList autorVorname = doc.getElementsByTagName("vorname");
			Node vorname = autorVorname.item(0);
			
			NodeList autorNachname = doc.getElementsByTagName("nachname");
			Node nachname = autorNachname.item(0);
			
			System.out.println("titel: " + titel.getTextContent() + "\nvorname: " + vorname.getTextContent() + "\nnachname: " + nachname.getTextContent());
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		} 

	}

}
