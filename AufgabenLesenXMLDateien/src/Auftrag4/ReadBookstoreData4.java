package Auftrag4;

import javax.xml.parsers.*;
import org.w3c.dom.*;

/* Arbeitsauftrag:  Lesen Sie nur die Buchtitel 
 *					aus der Datei "buchhandlung.xml" und geben Sie sie 
 * 					auf dem Bildschirm aus.
 * 
 *                  Ausgabe soll wie folgt formatiert werden:
 *                     1. titel: Everyday Italian
 *                     2. titel: Harry Potter
 *                     3. titel: XQuery Kick Start
 *                     4. titel: Learning XML
 *                     
 * Hinweis: Sie ben�tigen ein NodeList-Objekt und eine Schleife, die diese iteriert!
 */



public class ReadBookstoreData4 {

	public static void main(String[] args) {

			// Name der Datei: "src/Auftrag4/buchhandlung.xml"
		try {
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("src/Auftrag4/buchhandlung.xml");
			NodeList titelListe = doc.getElementsByTagName("titel");
			
			for(int i = 0; i < titelListe.getLength(); i++) {
				Node titel = titelListe.item(i);
				System.out.println("titel: " + titel.getTextContent());
				
			};
			
			
			
			
			
			
		}catch (Exception e){
			e.printStackTrace();			
		}
			
			



	}

}
