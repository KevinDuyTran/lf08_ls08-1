package Auftrag2;

 /* Arbeitsauftrag:  Lesen Sie den Titel und den Autor des Buches "Java ist auch eine Insel" 
 *					 aus der Datei "buchhandlung.xml" und geben Sie sie auf dem Bildschirm aus.
 *
 *                   Ausgabe soll wie folgt aussehen:
 *                        titel:  Java ist auch eine Insel 
 *                        autor:  Max Mustermann 
 */


import javax.xml.parsers.*;
import org.w3c.dom.*;

public class ReadBookstoreData2 {

	public static void main(String[] args) {

		try {
			// Name der Datei: "src/Auftrag2/buchhandlung.xml"
			DocumentBuilderFactory factory =  DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse("src/Auftrag2/buchhandlung.xml");
			
			NodeList titelListe = doc.getElementsByTagName("titel");
			Node titel = titelListe.item(0);
			NodeList autorListe = doc.getElementsByTagName("autor");
			Node autor = autorListe.item(0);
			
			System.out.println("titel: " + titel.getTextContent() + "\n" + "autor: " + autor.getTextContent());
			
		} catch (Exception e) {
			e.printStackTrace();
		} 

	}

}
