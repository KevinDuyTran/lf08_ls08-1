package Auftrag1;

import javax.xml.parsers.*;
import org.w3c.dom.*;


public class ReadBookstoreData1 {

  public static void main(String[] args) {

    try {
    	// Name der Datei: "src/Auftrag1/buchhandlung.xml"
    	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    	DocumentBuilder builder = factory.newDocumentBuilder();
    	
    	Document doc = builder.parse("src/Auftrag1/buchhandlung.xml");
    	
    	doc.getElementsByTagName("titel");
    	NodeList titelListe = doc.getElementsByTagName("titel");
    	Node titel = titelListe.item(0);
    	
    	System.out.println(titel.getTextContent());
    	

    } catch (Exception e) {
      e.printStackTrace();
    } 

  }

}
