
/**
 * @author dariush
 *
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.io.FileReader;
import java.io.IOException;

public class CSVHandler {

	private String file = "studentName.csv"; // muss sich im ProjektOrdner befinden!
	private String delimiter = ";";
	private String line = "";

	// Constructor 1
	public CSVHandler() {
	}

	// Constructor 2
	public CSVHandler(String delimiter, String file) {
		super();
		this.delimiter = delimiter;
		this.file = file;
	}

	// Begin Methods

	public List<Schueler> getAll() {
		Schueler s = null;
		List<Schueler> students = new ArrayList<Schueler>();

		
		
		// Add your code here
		
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader reader = new BufferedReader(fr);
			
			String zeile = reader.readLine();
			
			
			while (zeile != null) {
				String[] wordlist = zeile.split(this.delimiter);
				int timer = 0;
				for (String value: wordlist) {
					System.out.printf("%-10s" , value);
					
					if(timer == 4) {
						System.out.println();
						timer = 0;
					}
					timer++; 
				} 
				zeile = reader.readLine();
			}
			
			
			
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println();
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader reader = new BufferedReader(fr);
			
			line = reader.readLine();
			line = reader.readLine();
			while(line != null) {
				String[] value = line.split(this.delimiter);
				
				students.add(new Schueler(value[0] + " " + value[1] , Integer.parseInt(value [2] ), Integer.parseInt(value [3] ), Integer.parseInt(value [4] )));
				line = reader.readLine();
			}
		}catch (Exception e) {
			System.out.println(e.toString());
			}
		

		
		
		return students;
	}

	public void printAll(List<Schueler> students) {
		for (Schueler s : students) {
			System.out.println(s.getName());
		}
	}
}