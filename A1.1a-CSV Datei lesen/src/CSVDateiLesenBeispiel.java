import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;

public class CSVDateiLesenBeispiel {

	public static void main(String[] args) {
		
		try {
			FileReader fr = new FileReader("artikelliste.csv");
			
			BufferedReader reader = new BufferedReader(fr);
			
			String zeile = reader.readLine();
			
			while ( zeile != null) {
				System.out.println(zeile);
				zeile = reader.readLine();
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
