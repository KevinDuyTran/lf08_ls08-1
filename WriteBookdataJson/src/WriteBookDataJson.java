import java.io.FileWriter;
import java.util.*;

import javax.json.*;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/*
{
	"titel":"Java ist auch eine Insel!"
	"autoren":[
				"autor1",
				"autor2",
				"autor3"
				]
}

*/
public class WriteBookDataJson {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<String> aListe = new ArrayList<String>();
		Buch b1 = new Buch("Java ist auch eine Insel", aListe);
		
		JsonObjectBuilder builder = Json.createObjectBuilder();
		builder.add("titel",b1.getTitel());
		
		JsonArrayBuilder autorArray = Json.createArrayBuilder();
		autorArray.add("autor1");
		autorArray.add("autor2");
		autorArray.add("autor3");
		builder.add("autoren", autorArray);
		
		JsonObject jo = builder.build();
		
		try {
			FileWriter fw = new FileWriter("book1.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(jo);
			fw.close();
			jw.close();
			
		}catch(Exception ex){
			ex.printStackTrace();			
		}
		

	}

}
