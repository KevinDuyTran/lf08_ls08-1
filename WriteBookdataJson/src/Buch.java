import java.util.ArrayList;

public class Buch {
	private String titel;
	private ArrayList<String> autorListe;
	
	public Buch(String titel, ArrayList autorListe) {
		this.titel = titel;
		this.autorListe = autorListe;
	}
	
	public String getTitel() {
		return this.titel;
	}
	
	public ArrayList<String> getAutorListe(){
		return this.autorListe;
	}

}
