/* Arbeitsauftrag:  Erstellen Sie ein DOM-Dokument gem�� den Vorgaben 
 * 					aus der Datei "Vorgabe_f�r_Ausgabedatei.xml" 
 * 					und sichern Sie es als XML in eine Datei 
 * 					mit dem Filename "buchhandlung.xml".
 * 	
 *               
 */
package Auftrag2;
import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class WriteBookstoreData2 {

	public static void main(String[] args) {

		//Add your code here
try {
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.newDocument();
			
			
			//Erstellen & Appenden der einzelnen Elemente
			Element buchhandlung = doc.createElement("buchhandlung");
			doc.appendChild(buchhandlung);
			
			Element buch = doc.createElement("buch");
			buchhandlung.appendChild(buch);
			
			Element titel = doc.createElement("titel");
			buch.appendChild(titel);
			
			titel.appendChild(doc.createTextNode("Java ist auch eine Insel"));
			
			Element autor = doc.createElement("autor");
			buch.appendChild(autor);
			autor.appendChild(doc.createTextNode("Christian Ullenboom"));
			
			
			 //Transforming Prozess
			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();
			
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("src/Auftrag2/buchhandlung.xml"));
			
			transformer.transform(source, result);
			
		}catch (Exception ex){
			ex.printStackTrace();
		}

	}

}
