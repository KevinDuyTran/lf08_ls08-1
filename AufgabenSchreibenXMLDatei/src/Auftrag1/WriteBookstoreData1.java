package Auftrag1;
/* Arbeitsauftrag:  Erstellen Sie ein DOM-Dokument gem�� den Vorgaben 
 * 					aus der Datei "Vorgabe_f�r_Ausgabedatei.xml" 
 * 					und sichern Sie es als XML in eine Datei 
 * 					mit dem Filename "buchhandlung.xml".
 * 	
 *               
 */

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;
import java.io.File;

public class WriteBookstoreData1 {

	public static void main(String[] args) {

		//Add your code here
		try {
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.newDocument();
			
			
			//Erstellen & Appenden der einzelnen Elemente
			Element buchhandlung = doc.createElement("buchhandlung");
			doc.appendChild(buchhandlung);
			
			Element buch = doc.createElement("buch");
			buchhandlung.appendChild(buch);
			
			Element titel = doc.createElement("titel");
			buch.appendChild(titel);
			
			titel.appendChild(doc.createTextNode("Java ist auch eine Insel"));
			
			
			 //Transforming Prozess
			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer();
			
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("src/Auftrag1/buchhandlung.xml"));
			
			transformer.transform(source, result);
			
		}catch (Exception ex){
			ex.printStackTrace();
		}
		
	}

}
